import React from 'react';

import { Redirect, Route, Switch } from 'react-router';
import { BrowserRouter as Router } from 'react-router-dom';
import AddressSearchPage from './components/AddressSearchPage';
import LoginPage from './components/LoginPage';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { faMapMarkedAlt, faSearch, faTimes, faCircle, faCog } from '@fortawesome/free-solid-svg-icons';

library.add(fas, faMapMarkedAlt, faSearch, faTimes, faCircle, faCog);

const App = () => (
  <Router>
    <Switch>
      <Route path='/sign-in' component={LoginPage} />
      <Route path='/address' component={AddressSearchPage} />
      <Redirect to='/sign-in' />
    </Switch>
  </Router>
);

export default App;
