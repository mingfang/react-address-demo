import React, { useState, useMemo, useEffect } from 'react';
import { path } from 'ramda';
import axios from 'axios';

const getInputChange = (setState: (s: any) => void, key: string) => (event: object) => {
  const value = path(['target', 'value'], event);
  setState((state: object) => ({ ...state, [key]: value }));
};

const tryLogin = (setState: (s: any) => void, uid: string, pass: string, remembe: boolean, history: object) => {
  setState((state: object) => ({ ...state, isLoading: true }));
  axios
    .post('https://vae8mfb0oi.execute-api.ap-southeast-2.amazonaws.com/prod/v1/sign-in', { uid, pass })
    .then(({ data }) => {
      const accessToken = data['access_token'];
      if (remembe) {
        window.localStorage.setItem('accessToken', accessToken);
      }
      window.sessionStorage.setItem('accessToken', accessToken);
      setState((state: object) => ({ ...state, isLoading: false, errMsg: '' }));
      //@ts-ignore
      history.push('/address');
    })
    .catch(() => {
      setState((state: object) => ({ ...state, isLoading: false, errMsg: 'Login failed' }));
    });
};

const LoginPage = (props: { history: object }) => {
  const { history } = props;
  const [state, setState] = useState({
    passwd: '',
    uid: '',
    remember: false,
    errMsg: '',
    isLoading: false
  });
  const { uid, passwd, remember, errMsg, isLoading } = state;
  const onUserChg = useMemo(() => getInputChange(setState, 'uid'), []);
  const onPassChg = useMemo(() => getInputChange(setState, 'passwd'), []);
  const onRemeberChg = useMemo(() => getInputChange(setState, 'remember'), []);

  useEffect(() => {
    //wramup call
    axios.get('https://vae8mfb0oi.execute-api.ap-southeast-2.amazonaws.com/prod/v1/status');
  }, []);

  return (
    <div className='site-login'>
      <div className='container-fluid'>
        <div className='row'>
          <div className='col-md-4 login-panel-left'>
            <div className='row logo-area col-md-12'>
              <div>
                <img src='/images/devoli-logo.svg' />
                <h1 className='text-center'>Address Search</h1>
              </div>
              {/* <div className="col-md-4 col-xs-4">
                <img className="img-responsive" src="/images/front-vumeda.gif" />
              </div> */}
            </div>
          </div>
          <div className='col-md-8 login-panel-right'>
            <div className='transparent-panel center'>
              <div className='row'>
                <div className='col-md-12'>
                  <div className='login-heading'>
                    <h1>LOGIN</h1>
                  </div>
                  <form id='login-form'>
                    <div className='row'>
                      <div className='col-md-12'>
                        <div className='form-group field-loginform-username required'>
                          <label className='control-label' htmlFor='loginform-username'>
                            Username
                          </label>
                          <input type='text' id='loginform-username' onChange={onUserChg} className='form-control' aria-required='true' />
                          <p className='help-block help-block-error' />
                        </div>
                        <div className='form-group field-loginform-password required'>
                          <label className='control-label' htmlFor='loginform-password'>
                            Password
                          </label>
                          <input
                            type='password'
                            id='loginform-password'
                            className='form-control'
                            aria-required='true'
                            onChange={onPassChg}
                          />
                          <p className='help-block help-block-error' />
                        </div>
                      </div>

                      {/* <div className="col-md-12">
                        <div className="pull-left">
                          <div className="form-group field-loginform-rememberme">
                            <div className="checkbox">
                              <input
                                type="checkbox"
                                onChange={onRemeberChg}
                                id="loginform-rememberme"
                                name="LoginForm[rememberMe]"
                                value="0"
                              />
                              <label htmlFor="loginform-rememberme">Remember Me</label>
                              <p className="help-block help-block-error" />
                            </div>
                          </div>
                        </div>
                      </div> */}
                      {errMsg && (
                        <div className='col-md-12'>
                          <div className='login-failed'>Login failed</div>
                        </div>
                      )}
                      <div className='col-md-12 col-sm-12'>
                        <div className='form-group'>
                          <button
                            type='button'
                            disabled={!uid || !passwd || isLoading}
                            className='btn btn-primary'
                            name='login-button'
                            onClick={() => tryLogin(setState, uid, passwd, remember, history)}
                          >
                            Login&nbsp;&nbsp;
                            {isLoading && <span className='spinner-border spinner-border-sm' role='status' aria-hidden='true' />}
                          </button>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
