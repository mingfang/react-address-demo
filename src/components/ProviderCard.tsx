import React, { useState } from 'react';
import { ImagesUrl } from '../constants';
import { IpdctsProps } from '../interfaces';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { CSSTransition } from 'react-transition-group';

const { telstraLogo, aaptLogo, vocusLogo, nbnLogo, optusLogo, loadingLogo } = ImagesUrl;
type LogoType = { [key: string]: string };

const logos: LogoType = {
  telstra: telstraLogo,
  aapt: aaptLogo,
  vocus: vocusLogo,
  nbn: nbnLogo,
  optus: optusLogo,
  loading: loadingLogo
};
const trimOutBracket = (txt: string) => txt.replace(/\(([^}]*)\)/, '').trim();

const providerInfo = ({ isLoading, products }: IpdctsProps) =>
  isLoading
    ? 'Wait a second'
    : products === 'err'
    ? 'No Response, please try again.'
    : Array.isArray(products)
    ? products.length > 0
      ? 'Please check products below'
      : 'Sorry No product available'
    : 'Please Contact us for more info';

const withProductCard = (provider: string, providerStatus: string, isLoading: boolean) => (
  <CSSTransition timeout={500} classNames='fade' in={isLoading}>
    <div className='col-md-6 mb-1 p-2' style={{ display: 'none' }}>
      <div className='card mb-4 shadow bg-light h-100'>
        <div className='card-img-top shadow-sm bg-white h-100 flex-center-parent'>
          <img src={logos[provider]} alt={provider + ' Logo'} style={{ objectFit: 'contain', width: '100%' }} />
        </div>
        <div className='p-3'>
          <div className='shadow-sm bg-white rounded p-2'>
            <div className='card-info'>
              <div className='clearfix'>
                <div className='card-status px-2 float-left '>
                  <h5>Provider</h5>
                  <p>{provider}</p>
                </div>
                <div className='card-status px-2 float-right text-right'>
                  <h5>Status</h5>
                  <p>{providerStatus}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </CSSTransition>
);

const withoutProduct = ({ provider, products }: IpdctsProps, providerStatus: string, isLoading: boolean) => (
  <CSSTransition timeout={500} classNames='fade' in={isLoading}>
    <div className='col-md-6 mb-1 p-2' style={{ display: 'none' }}>
      <div className='card shadow bg-light'>
        <div className='row p-3'>
          <div className='col-6'>
            <div className='shadow-sm bg-white'>
              <img src={logos[provider]} alt={provider + ' Logo'} style={{ objectFit: 'contain', width: '100%' }} />
            </div>
          </div>
          <div className='col-6'>
            <div className='shadow-sm rounded bg-white h-100 flex-center-parent'>
              <div className='card-status px-3 text-right'>
                <h5>Status</h5>
                <p>{providerStatus}</p>
              </div>
            </div>
          </div>
          <div className='col-12 mt-3'>
            <div className='shadow-sm card-status-title rounded bg-white px-3'>
              <div className='py-2'>
                <div className='row'>
                  <div className='card-title col-6'>Products</div>
                </div>
                <div className='card-product-list'>
                  {Array.isArray(products) &&
                    products.map((productName, index) => (
                      <div className='py-1 px-2 mt-2 rounded product-item' key={index}>
                        {trimOutBracket(productName)}
                      </div>
                    ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </CSSTransition>
);

const loadingProduct = ({ provider }: IpdctsProps, providerStatus: string, isLoading: boolean) => (
  <CSSTransition timeout={500} classNames='fade' in={isLoading}>
    <div className='col-md-6 mb-1 p-2'>
      <div className='card shadow bg-light h-100'>
        <div className='p-3 h-100'>
          <div className='card-img-top shadow-sm bg-white flex-center-parent h-100'>
            <img src={logos[provider]} alt={provider + ' Logo'} style={{ objectFit: 'contain', width: '100%' }} />
          </div>
          <div className='status-area'>
            <FontAwesomeIcon icon='cog' className='spinning' />
            <span>Loading...</span>
          </div>
        </div>
      </div>
    </div>
  </CSSTransition>
);

const ProviderCard = ({ resultItem }: { resultItem: IpdctsProps }) => {
  const { products, provider, isLoading } = resultItem;
  const hasProducts = Array.isArray(products) && products.length > 0;
  const providerStatus = isLoading ? 'Loading' : products === 'err' ? 'No Response' : hasProducts ? 'Available' : 'Confirm Required';

  return (
    <>
      {loadingProduct(resultItem, providerStatus, isLoading)}
      {!hasProducts ? withProductCard(provider, providerStatus, !isLoading) : withoutProduct(resultItem, providerStatus, !isLoading)}
    </>
  );
};

export default ProviderCard;
