import React, { useEffect, useMemo } from 'react';
import Search from './Search';
import MapBox from './MapBox';
import logo from '../assets/images/devoli-logo.svg';
import { validToken } from '../utils';
import { StateProvider } from './Context';

const initState = {
  latitude: -33.86708068847656,
  longitude: 151.2056121826172
};

const reducer = (state, action) => {
  const { type, latitude, longitude } = action;
  switch (type) {
    case 'UPDATE_GEO':
      return { latitude, longitude };
    default:
      throw new Error();
  }
};

const checkTokenValidality = history => () => {
  const accessToken = window.sessionStorage.getItem('accessToken') || window.localStorage.getItem('accessToken');
  if (!validToken(accessToken)) {
    history.push('/sign-in');
    console.log('token expires, redirect to sign in');
  }
};

const App = props => {
  const { history } = props;
  const validateToken = useMemo(() => checkTokenValidality(history), [history]);

  useEffect(() => {
    validateToken();
    const ref = setInterval(validateToken, 5000);
    return () => clearInterval(ref);
  }, []);

  const logout = () => {
    window.sessionStorage.removeItem('accessToken');
    history.push('/sign-in');
  };

  return (
    <>
      <nav className='navbar navbar-expand-md py-md-4 navbar-light fixed-top' style={{ position: 'absolute' }}>
        <div className='container-fluid'>
          <a className='navbar-brand' href='/'>
            <img src={logo} alt='Devoli' />
          </a>
          <ul className='ml-auto navbar-nav'>
            <li className='nav-item'>
              <a href='.' className='nav-link active'>
                Address Search
              </a>
            </li>
            <li className='nav-item'>
              <a href='https://vumeda.devoli.com' className='nav-link'>
                Vumeda
              </a>
            </li>
            <li className='nav-item'>
              <a href='/' className='nav-link' onClick={logout}>
                Log Out
              </a>
            </li>
          </ul>
        </div>
      </nav>
      <section className='address-section'>
        <StateProvider initState={initState} reducer={reducer}>
          <div className='main-panel'>
            <div className='container'>
              <Search />
            </div>
          </div>
          <div className='side-panel'>
            <div className='container'>
              <MapBox />
            </div>
          </div>
        </StateProvider>
      </section>
      <footer className='as-footer'>
        <div className='container-fluid'>
          <p>© Copyright 2019 Devoli All Rights Reserved</p>
        </div>
      </footer>
    </>
  );
};

export default App;
