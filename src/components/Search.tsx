import React, { Component } from 'react';
import axios from 'axios';
import { path } from 'ramda';
import { Endpoints } from '../constants';
import { AsyncTypeahead } from 'react-bootstrap-typeahead';
import { Tcompany, IpdctsProps } from '../interfaces';
import ProviderCard from './ProviderCard';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { debounce } from '../utils';
import { MainContext } from './Context';

interface Iselected {
  address: string;
  providers: string[];
  hash: string;
  geo: { lat: number; lon: number };
}
interface IsearchResult {
  data: { result: Iselected[] };
}
interface ISearchState {
  isLoading: boolean;
  options: Iselected[];
  address: string;
  resultItems: IpdctsProps[];
}
interface ISearchProp {
  handleSelect: Function;
}

const serviceProviders: Tcompany[] = ['devoli', 'telstra', 'nbn', 'aapt'];

const MenuItem = ({ item }: any) => (
  <div className='item'>
    <div className='streetAddress'>{item.address}</div>
  </div>
);

class Search extends Component<ISearchProp, ISearchState> {
  constructor(props: any) {
    super(props);
    this.state = {
      isLoading: false,
      options: [] as Iselected[],
      address: '' as string,
      resultItems: [] as IpdctsProps[]
    };
  }

  accessToken: string | null = '';
  resultItems: IpdctsProps[] = [];
  addressSearch: any;

  static contextType = MainContext;

  componentDidMount() {
    this.accessToken = window.sessionStorage.getItem('accessToken');
  }

  getSearchResult = (endpoint: string, keywords: object, successCb: any, failCb: (error: any) => {}) =>
    axios
      .get(endpoint, {
        params: { country: 'AU', ...keywords },
        headers: { Authorization: `Bearer ${this.accessToken}` }
      })
      .then(successCb)
      .catch(failCb);

  //@ts-ignore
  searchFn = (...args) => debounce(this.getSearchResult(...args), 1000);

  setProductLoadingStatus = (name: string, isLoading: false, failed: boolean = false) =>
    this.setState({
      resultItems: this.state.resultItems.map(item => {
        const { provider, products: productsItem } = item;
        if (failed) {
          const products = 'err';
          return provider === name ? { ...item, isLoading, products } : item;
        } else {
          const products = productsItem;
          return provider === name ? { ...item, isLoading, products } : item;
        }
      })
    });

  getProducts = (name: Tcompany, hash: string) => {
    if (hash) {
      const { address } = this.state;
      this.searchFn(
        Endpoints.ProductList + `/${name}/${hash}`,
        { address },
        ({ data }: IsearchResult) => {
          const products: IpdctsProps[] = path(['result', 'list'], data) || [];
          products.forEach(item => {
            const { provider } = item;
            this.resultItems = this.state.resultItems.map(resultItem => {
              const { provider: resultItemProvider } = resultItem;
              return resultItemProvider === provider ? { ...item, isLoading: false } : resultItem;
            });
            this.setState({ resultItems: this.resultItems });
          });
        },
        (error: any) => {
          console.error(error);
          this.setProductLoadingStatus(name, false, true);
        }
      );
    } else {
      this.setProductLoadingStatus(name, false);
    }
  };

  searchAutoComplete = (address: string) => {
    this.setState({ isLoading: true, address });
    this.searchFn(
      Endpoints.DevoliSearch,
      { address },
      ({ data }: IsearchResult) => {
        const { result: options } = data;
        this.setState({ options, isLoading: false });
      },
      (error: any) => {
        this.setState({ isLoading: false });
        console.error(error);
      }
    );
  };

  searchByProviders = (selectedItem: Iselected) => {
    const { providers: dProvider, hash: devoliHash, address, geo } = selectedItem;
    const providers = dProvider ? [...dProvider, 'telstra', 'nbn', 'aapt'] : ['telstra', 'nbn', 'aapt'];
    const resultItems = providers.sort().map(provider => ({ products: [], provider, isLoading: true }));
    this.setState({ address, resultItems });

    serviceProviders.forEach(name => {
      if (name === 'devoli') {
        dProvider && Array.isArray(dProvider) && dProvider.length > 0 && this.getProducts(name, devoliHash);
      } else {
        this.searchFn(
          Endpoints.ExternalSearch + `/${name}`,
          { address },
          ({ data }: IsearchResult) => {
            const { result } = data;
            const { hash } = result.length > 0 ? result[0] : ({} as { hash: string; providers: string[] });
            this.getProducts(name, hash);
          },
          (error: any) => {
            console.error(error);
            this.setProductLoadingStatus(name, false, true);
          }
        );
      }
    });
  };

  updateGeo = ({ lat, lon }: { lat: number; lon: number }) => {
    const [state, dispatch] = this.context;
    dispatch({
      type: 'UPDATE_GEO',
      latitude: lat,
      longitude: lon
    });
  };

  handleOnChange = ([selectedItem]: Array<Iselected>) => {
    if (selectedItem) {
      const { geo } = selectedItem;
      this.searchByProviders(selectedItem);
      //@ts-ignore
      geo && debounce(this.updateGeo(geo), 5000);
    }
  };

  handleSearch = (address: string) => this.searchAutoComplete(address);

  render() {
    const { resultItems, options, isLoading } = this.state;
    const hasResult = resultItems.length > 0;

    return (
      <div className={(hasResult ? '' : 'no-result') + ' address-search animate-node'}>
        <div className='row row-eq-height'>
          <div className='col-12 p-2'>
            <h1 className='display-3 animate-node'>Address Search</h1>
            <h5 style={{ color: '#fff' }}>
              <FontAwesomeIcon icon='map-marker-alt' style={{ marginRight: '8px' }} />
              Search for a location
            </h5>
            <div className='search-bar mb-4'>
              <AsyncTypeahead
                options={options}
                highlightOnlyResult={true}
                isLoading={isLoading}
                labelKey='address'
                id='address-search-bar'
                searchText='Searching now...'
                placeholder='49 YORK STREET SYDNEY NSW 2000 AUSTRALIA'
                promptText='Search for a place'
                minLength={3}
                defaultInputValue='49 YORK STREET SYDNEY'
                onChange={this.handleOnChange}
                onSearch={this.handleSearch}
                renderMenuItemChildren={option => <MenuItem position={option.hash} item={option} />}
                filterBy={() => true}
                ref={ref => (this.addressSearch = ref)}
              />
              <button className='btn btn-secondary btn-clear' onClick={() => this.addressSearch.getInstance().clear()}>
                <FontAwesomeIcon icon='times' />
              </button>
            </div>
          </div>
          {resultItems.map(resultItem => (
            <ProviderCard resultItem={resultItem} />
          ))}
        </div>
      </div>
    );
  }
}

export default Search;
