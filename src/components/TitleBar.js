import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const BaseDiv = styled.div`
  margin-bottom: 1.5rem;
`;

const TopLine = styled(BaseDiv)`
  border-top: 2px solid #ccc;
`;

const BottomLine = styled(BaseDiv)`
  border-bottom: 2px solid #ccc;
`;

const TitleBar = ({ title, bottomLine, topLine }) => {
  const Wrapper = bottomLine ? BottomLine : topLine ? TopLine : BaseDiv;
  return title ? (
    <Wrapper>
      <label>
        <h4>{title}</h4>
      </label>
    </Wrapper>
  ) : null;
};

TitleBar.propTypes = {
  title: PropTypes.string,
  bottomLine: PropTypes.bool,
  topLine: PropTypes.bool
};

export default TitleBar;
