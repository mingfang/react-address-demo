import React, { createContext, useContext, useReducer } from 'react';
export const MainContext = createContext(null);
//@ts-ignore
export const StateProvider = ({ reducer, initState, children }) => (
  //@ts-ignore
  <MainContext.Provider value={useReducer(reducer, initState)}>{children}</MainContext.Provider>
);
export const useStateValue = () => useContext(MainContext);
