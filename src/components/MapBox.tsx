import React, { useState, useContext, useEffect } from 'react';
import ReactMapGL, { FlyToInterpolator, Marker } from 'react-map-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import { MainContext, useStateValue } from './Context';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { debounce } from '../utils';

const MapBox = () => {
  //@ts-ignore
  const [{ longitude, latitude }] = useStateValue();

  const [viewport, setViewport] = useState({
    width: '100%',
    height: '60vh',
    latitude,
    longitude,
    zoom: 8
  });

  useEffect(() => {
    setViewport({
      ...viewport,
      longitude,
      latitude,
      zoom: 14,
      //@ts-ignore
      transitionDuration: 3000,
      transitionInterpolator: new FlyToInterpolator()
    });
  }, [latitude]);

  return (
    <div className='location-wrap'>
      <h1 className='display-3'>Location</h1>
      <div className='mapbox mt-4 shadow p-3 bg-white rounded'>
        <ReactMapGL
          {...viewport}
          mapboxApiAccessToken={process.env.REACT_APP_MAP_BOX_TOKEN}
          mapStyle='mapbox://styles/mingfangdev/cjwb8olo71rgc1dqbx8c9b31d'
          onViewportChange={viewport => {
            //@ts-ignore
            setViewport(viewport);
          }}
        >
          <Marker latitude={latitude} longitude={longitude} offsetLeft={45} offsetTop={-25}>
            <FontAwesomeIcon icon='map-marker-alt' style={{ color: '#f47a1f', fontSize: '20px' }} className='map-marker' />
          </Marker>
        </ReactMapGL>
      </div>
    </div>
  );
};
export default MapBox;
