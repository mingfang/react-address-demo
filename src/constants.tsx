import telstraLogo from './assets/images/logo/telstra.png';
import aaptLogo from './assets/images/logo/aapt.png';
import vocusLogo from './assets/images/logo/vocus.png';
import nbnLogo from './assets/images/logo/nbn.png';
import optusLogo from './assets/images/logo/optus.png';
import loadingLogo from './assets/images/logo/loading.gif';

const { hostname } = window.location;
const root = process.env.NODE_ENV === 'development' ? '/api' : 'https://' + hostname + '/api';

const DevoliSearch = `${root}/address/complex/devoli`;
const TelstraSearch = `${root}/address/simple/telstra`;
const AaptSearch = `${root}/address/simple/aapt`;
const NbnSearch = `${root}/address/simple/nbn`;
const ProductList = `${root}/products/simple`;
const ExternalSearch = `${root}/address/complex`;

export const Endpoints = {
  DevoliSearch,
  TelstraSearch,
  AaptSearch,
  ExternalSearch,
  NbnSearch,
  ProductList
};

export const ImagesUrl = {
  telstraLogo,
  aaptLogo,
  vocusLogo,
  nbnLogo,
  optusLogo,
  loadingLogo
};
