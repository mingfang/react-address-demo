export const parseJWT = (token: string): object | null => {
  if (!token) {
    return null;
  }
  const base64Url = token.split('.')[1];
  const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
  return JSON.parse(window.atob(base64));
};

export const validToken = (token: string) => {
  //@ts-ignore
  const { exp } = parseJWT(token) || {};
  if (exp && new Date(exp * 1000) > new Date()) {
    return true;
  }
  return false;
};

export const debounce = (fn: (...args: any) => {}, delay: number) => {
  let time: number | null = null;
  const fnCall = (...args: any) => () => {
    fn(...args);
    time = -1;
  };

  return (...args: any) => {
    if (time) {
      window.clearTimeout(time);
    }
    time = window.setTimeout(fnCall(...args), time === null ? 1 : delay);
  };
};

export const sleep = (ms: number) => {
  return new Promise(resolve => setTimeout(resolve, ms));
};
