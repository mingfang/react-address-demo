export interface IpdctsProps {
  products: string[] | 'err';
  provider: string;
  isLoading: boolean;
}

export interface IaddrProps {
  providers: Array<string>;
  products: Tproduct;
  address: string;
  loading: boolean;
}

export interface Ilocation {
  longitude: number;
  latitude: number;
}

export type Tproduct = { [key: string]: { hash: string; isLoading: boolean } };
export type Tcompany = 'telstra' | 'nbn' | 'aapt' | 'devoli';
