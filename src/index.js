import React from 'react';
import ReactDOM from 'react-dom';

import './assets/scss/index.scss';
import 'react-bootstrap-typeahead/css/Typeahead-bs4.css';

import App from './App';

ReactDOM.render(<App />, document.getElementById('root'));
