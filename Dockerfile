FROM node:10.15.3-alpine

COPY ./ /app/
RUN mkdir /app/build
WORKDIR /app

RUN npm install --production